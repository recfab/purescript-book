module Test.MySolutions where

import Prelude
import Data.Array
  ( (:)
  , (..)
  , concat
  , head
  , filter
  , last
  , length
  , nubByEq
  , null
  , sort
  , sortWith
  , tail
  )
import Data.Array as Array
import Data.Maybe (fromMaybe, isJust, Maybe)
import Data.Foldable (foldl)
import Control.MonadZero (guard)
import Test.Examples (factors, allFiles)
import Data.Path
import Data.String (split, Pattern(..))

-- Note to reader: Add your solutions to this file
-- 4.1 Exercise Group - Recursion
isEven :: Int -> Boolean
isEven 0 = true

isEven 1 = false

isEven n = isEven (n - 2)

countEven :: Array Int -> Int
countEven [] = 0

countEven arr = (binarize (isEven h)) + (countEven t)
  where
  h = fromMaybe 1 $ head arr

  t = fromMaybe [] $ tail arr

  binarize true = 1

  binarize false = 0

squared :: Array Number -> Array Number
squared = map (\x -> x * x)

keepNonNegative :: Array Number -> Array Number
keepNonNegative = filter (\n -> n >= 0.0)

infix 8 filter as <$?>

keepNonNegativeRewrite :: Array Number -> Array Number
keepNonNegativeRewrite ns = (\n -> n >= 0.0) <$?> ns

-- 4.2 Exercise Group - Folds and Tail Recursion
isPrime :: Int -> Boolean
isPrime 0 = false

isPrime 1 = false

isPrime n = (==) 1 $ length $ factors n

cartesianProduct :: forall t. Array t -> Array t -> Array (Array t)
cartesianProduct xs ys = do
  a <- xs
  b <- ys
  pure [ a, b ]

triples :: Int -> Array (Array Int)
triples n = do
  c <- 1 .. n
  b <- 1 .. c
  a <- 1 .. b
  guard $ a * a + b * b == c * c
  pure [ a, b, c ]

factorize :: Int -> Array Int
factorize n = Array.reverse $ sort $ factorize' n
  where
  factorize' n' = do
    a <- concat $ factors n'
    guard $ isPrime a
    pure a

-- 4.3 Exercise Group - Folds and Tail Recursion
allTrue :: Array Boolean -> Boolean
allTrue = foldl (\acc b -> acc && b) true

fibTailRec :: Int -> Int
fibTailRec 0 = 1

fibTailRec 1 = 1

fibTailRec n = fib' 1 1 (2 .. n)
  where
  fib' i j ns =
    if null ns then
      1
    else
      i + (fib' j (i + j) $ fromMaybe [] $ tail ns)

reverse :: forall a. Array a -> Array a
reverse = foldl (\acc x -> x : acc) []

-- 4.4 Exercise Group - Filesystem
onlyFiles :: Path -> Array Path
onlyFiles path =
  if isDirectory path then do
    child <- ls path
    onlyFiles child
  else
    [ path ]

whereIs :: Path -> String -> Maybe Path
whereIs haystack needle = head $ whereIs' $ allFiles haystack
  where
  basename file' = fromMaybe "" $ last $ split (Pattern "/") $ filename file'

  hasBasename basename' file' = eq basename' $ basename file'

  whereIs' haystacks = do
    path <- haystacks
    child <- ls path
    guard $ hasBasename needle child
    pure path

largestSmallest :: Path -> Array Path
largestSmallest path = nubByEq samePath largestSmallest'
  where
  fromJust = fromMaybe $ File "" 0

  sortedPaths = sortWith size $ onlyFiles path

  samePath a b = filename a == filename b && size a == size b

  largestSmallest' = do
    path' <- [ last sortedPaths, head sortedPaths ]
    guard $ isJust path'
    pure $ fromJust path'
