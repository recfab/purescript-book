module Test.MySolutions where

import Prelude
import Data.Array ((..), head, tail, null)
import Data.Maybe (fromMaybe, Maybe(..))
import Data.Person (Person)
import Data.Picture (Shape(..), origin, getCenter)
import Data.Picture as P
import ChapterExamples
import Math as Math

-- 5.1 Exercise Group - Simple Pattern Matching
factorial :: Int -> Int
factorial 0 = 1

factorial 1 = 1

-- factorial n = n * factorial (n-1)
factorial n = factorial' 1 (n .. 1)
  where
  head' xs = fromMaybe 1 $ head xs

  tail' xs = fromMaybe [] $ tail xs

  factorial' acc xs
    | null xs = acc
    | otherwise = factorial' (acc * (head' xs)) (tail' xs)

-- binomial coefficient i.e. choose `k` items from set of size `n`
-- n! / k! (n - k)!
binomial :: Int -> Int -> Int
binomial 0 _ = 0

binomial n k = (factorial n) / ((factorial k) * (factorial (n - k)))

-- Pascal's Rule for calculating binomial coefficient
-- (n-1 choose k) + (n-1 choose k-1) = (n choose k)
pascal :: Int -> Int -> Int
pascal n k = pascal' 0 n k
  where
  pascal' acc 0 0 = acc + 1

  pascal' acc 0 _ = acc

  pascal' acc n' k'
    | n' < k' = acc
    | otherwise = (pascal' acc (n' - 1) k') + (pascal' acc (n' - 1) (k' - 1))

-- 5.2 Exercise Group - Array and Record Patterns
sameCity :: Person -> Person -> Boolean
sameCity { address: { city: city1 } } { address: { city: city2 } } = city1 == city2

fromSingleton :: forall a. a -> Array a -> a
fromSingleton _ [ x ] = x

fromSingleton d _ = d

-- 5.3 Exercise Group - Algebraic Data Types
circleAtOrigin :: Shape
circleAtOrigin = Circle { x: 0.0, y: 0.0 } 10.0

center :: Shape -> Shape
center (Circle _ r) = Circle origin r

center (Rectangle _ h w) = Rectangle origin h w

center (Text _ t) = Text origin t

center (Line p q) =
  let
    c = getCenter (Line p q)
  in
    Line
      { x: p.x - c.x, y: p.y - c.y }
      { x: q.x - c.x, y: q.y - c.y }

center (Clipped p _ h w) = Clipped p origin h w

scale :: Number -> Shape -> Shape
scale n (Circle p r) = Circle p (n * r)

scale n (Rectangle p h w) = Rectangle p (n * h) (n * w)

scale n (Text p t) = Text p t

scale n (Line { x: a, y: b } { x: c, y: d }) = Line { x: n * a, y: n * b } { x: n * c, y: n * d }

scale n (Clipped p c h w) = Clipped p c (n * h) (n * w)

doubleScaleAndCenter :: Shape -> Shape
doubleScaleAndCenter s = scale 2.0 $ center s

shapeText :: Shape -> Maybe String
shapeText (Text _ t) = Just t

shapeText _ = Nothing

-- 5.4 Exercise Group - Newtype
newtype Watt
  = Watt Number

calculateWattage :: Amp -> Volt -> Watt
calculateWattage (Amp a) (Volt v) = Watt (a * v)

-- 5.5 Exercise Group - Vector Graphics
area :: Shape -> Number
area (Line _ _) = 0.0
area (Text _ _) = 0.0
area (Circle _ r) = Math.pi * r * r
area (Rectangle _ w h) = h * w
area (Clipped p _ w h) = h * w
