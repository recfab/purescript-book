module Test.MySolutions where

import Prelude ((<<<), (==), (&&), not)
import Data.List (filter, head, null, nubByEq)
import Data.Maybe (Maybe)
import Data.AddressBook

-- Note to reader: Add your solutions to this file

findEntryByStreet :: String -> AddressBook -> Maybe Entry
findEntryByStreet street = head <<< filter filterEntry
  where
    filterEntry :: Entry -> Boolean
    filterEntry entry = entry.address.street == street

isInBook :: String -> String -> AddressBook -> Boolean
isInBook firstName lastName = not null <<< filter filterEntry
  where
    filterEntry :: Entry -> Boolean
    filterEntry entry = entry.firstName == firstName && entry.lastName == lastName

removeDuplicates :: AddressBook -> AddressBook
removeDuplicates = nubByEq sameNames
  where
    sameNames :: Entry -> Entry -> Boolean
    sameNames entryA entryB = entryA.firstName == entryB.firstName && entryA.lastName == entryB.lastName
