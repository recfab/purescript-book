module Test.MySolutions where

import Prelude
import Data.Array (nubEq, nub, nubByEq, length)
import Data.Foldable (class Foldable, foldr, foldl, foldMap, maximum)
import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)
import Data.Newtype (wrap, over2, class Newtype)
import Data.Maybe (fromJust)
import Data.Monoid (power)
import Data.Hashable

-- 6.1 Exercise Group - Show Me
-- NOTE: in PureScript, type class instances are named to aid the readability of the generated JavaScript.
-- Note: Point is now a `newtype` (instead of a `type` synonym), which allows us to customize how to show it. Otherwise, we'd be stuck with the default `Show` instance for records.
newtype Point
  = Point
  { x :: Number
  , y :: Number
  }

instance showPoint :: Show Point where
  show (Point { x, y }) = "(" <> show x <> ", " <> show y <> ")"

-- 6.2 Exercise Group - Common Type Classes
newtype Complex
  = Complex
  { real :: Number
  , imaginary :: Number
  }

instance showComplex :: Show Complex where
  show (Complex { real, imaginary }) =
    let
      sign
        | imaginary >= 0.0 = "+"
        | otherwise = ""
    in
      show real <> sign <> show imaginary <> "i"

derive instance eqComplex :: Eq Complex
derive instance newtypeComplex :: Newtype Complex _

instance semiringComplex :: Semiring Complex where
  zero = wrap zero
  add = over2 Complex add
  mul = over2 Complex
        \ { real: r1, imaginary: i1 }
          { real: r2, imaginary: i2 }
        ->
          { real: r1 * r2 - i1 * i2
          , imaginary: r1 * i2 + r2 * i1
          }
  one = wrap one

derive newtype instance ringComplex :: Ring Complex

data Shape
  = Circle Point Number
  | Rectangle Point Number Number
  | Line Point Point
  | Text Point String

derive instance genericShape :: Generic Shape _

instance showShape :: Show Shape where
  show = genericShow

-- 6.3 Exercise Group - Constraints and Dependencies

data NonEmpty a = NonEmpty a (Array a)
derive instance eqNonEmpty :: Eq a => Eq (NonEmpty a)
derive instance genericNonEmpty :: Generic (NonEmpty a) _

instance showNonEmpty :: (Show a, Show (Array a)) => Show (NonEmpty a) where
  show = genericShow

instance semigroupNonEmpty :: Semigroup (Array a) => Semigroup (NonEmpty a) where
  append (NonEmpty x xs) (NonEmpty y ys) = NonEmpty x (xs <> [y] <> ys)

instance functorNonEmpty :: Functor Array => Functor NonEmpty where
  map f (NonEmpty x xs) = NonEmpty (f x) (map f xs)

data Extended a = Infinite | Finite a
derive instance eqExtended :: Eq a => Eq (Extended a)
instance ordExtended :: Ord a => Ord (Extended a) where
  compare Infinite Infinite = EQ
  compare Infinite _ = GT
  compare _ Infinite = LT
  compare (Finite a) (Finite b) = compare a b

instance foldableNonEmpty :: Foldable Array => Foldable NonEmpty where
  foldr fn acc (NonEmpty x xs) = foldr fn acc ([x] <> xs)
  foldl fn acc (NonEmpty x xs) = foldl fn acc ([x] <> xs)
  foldMap fn (NonEmpty x xs) = foldMap fn ([x] <> xs)

data OneMore f a = OneMore a (f a)
instance foldableOneMore :: Foldable f => Foldable (OneMore f) where
  foldr :: forall a b. (a -> b -> b) -> b -> OneMore f a -> b
  foldr fn acc (OneMore x xs) = fn x y
    where
      y = foldr fn acc xs
  foldl :: forall a b. (b -> a -> b) -> b -> OneMore f a -> b
  foldl fn acc (OneMore x xs) = foldl fn y xs
    where
      y = fn acc x
  foldMap :: forall a m. Monoid m => (a -> m) -> OneMore f a -> m
  foldMap fn (OneMore x xs) = (fn x) <> foldMap fn xs

dedupShapes :: Array Shape -> Array Shape
dedupShapes = nubEq

derive instance eqPoint :: Eq Point
derive instance eqShape :: Eq Shape

derive instance ordPoint :: Ord Point
derive instance ordShape :: Ord Shape

dedupShapesFast :: Array Shape -> Array Shape
dedupShapesFast = nub

-- 6.4 Exercise Group - More or less than one Type argument

unsafeMaximum :: Partial => Array Int -> Int
unsafeMaximum arr = fromJust $ maximum arr

newtype Multiply = Multiply Int

instance semigroupMultiply :: Semigroup Multiply where
  append (Multiply n) (Multiply m) = Multiply (n * m)

instance monoidMultiply :: Monoid Multiply where
  mempty = Multiply 1

class Monoid m <= Action m a where
  act :: m -> a -> a
-- Laws:
-- act mempty a = a
-- act (m1 <> m2) a = act m1 (act m2 a)

instance actionMultiplyInt :: Action Multiply Int where
  act (Multiply m) n = m * n

instance actionMultiplyString :: Action Multiply String where
  act (Multiply n) s = power s n

instance actionArray :: Action m a => Action m (Array a) where
  act m arr = map (act m) arr

newtype Self m = Self m

derive newtype instance eqMultiply :: Eq Multiply
derive newtype instance eqSelf :: Eq m => Eq (Self m)

derive newtype instance showMultiply :: Show Multiply
derive newtype instance showSelf :: Show m => Show (Self m)

instance actionSelf :: Monoid m => Action m (Self m) where
  act :: m -> Self m -> Self m
  act m (Self n) = Self (m <> n)

-- 6.5 Exercise Group - Hashes
arrayHasDuplicates :: forall a. Hashable a => Hashable (Array a) => Array a -> Boolean
arrayHasDuplicates xs =
  let deduped = nubByEq eq' xs
  in length deduped < length xs
  where
    eq' a b = (hashEqual a b) && (eq a b)

newtype Hour = Hour Int

instance eqHour :: Eq Hour where
  eq (Hour n) (Hour m) = mod n 12 == mod m 12

instance hashHour :: Hashable Hour where
  hash (Hour h) = hash (h `mod` 12)
